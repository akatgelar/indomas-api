/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50516
 Source Host           : localhost:3306
 Source Schema         : indomas

 Target Server Type    : MySQL
 Target Server Version : 50516
 File Encoding         : 65001

 Date: 04/06/2018 12:46:07
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for core_store
-- ----------------------------
DROP TABLE IF EXISTS `core_store`;
CREATE TABLE `core_store`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `value` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `environment` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `type` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `tag` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `parent` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `parent`(`parent`) USING BTREE,
  CONSTRAINT `core_store_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `core_store` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of core_store
-- ----------------------------
INSERT INTO `core_store` VALUES (1, 'core_application', '{\"name\":\"Default Application\",\"description\":\"This API is going to be awesome!\"}', '', 'object', '', NULL);
INSERT INTO `core_store` VALUES (2, 'plugin_upload_provider', '{\"provider\":\"local\",\"name\":\"Local server\",\"enabled\":true,\"sizeLimit\":1000000}', 'development', 'object', '', NULL);
INSERT INTO `core_store` VALUES (3, 'plugin_users-permissions_grant', '{\"email\":{\"enabled\":true,\"icon\":\"envelope\"},\"facebook\":{\"enabled\":false,\"icon\":\"facebook-official\",\"key\":\"\",\"secret\":\"\",\"callback\":\"/auth/facebook/callback\",\"scope\":[\"email\"]},\"google\":{\"enabled\":false,\"icon\":\"google\",\"key\":\"\",\"secret\":\"\",\"callback\":\"/auth/google/callback\",\"scope\":[\"email\"]},\"github\":{\"enabled\":false,\"icon\":\"github\",\"key\":\"\",\"secret\":\"\",\"redirect_uri\":\"/auth/github/callback\",\"scope\":[\"user\",\"user:email\"]},\"twitter\":{\"enabled\":false,\"icon\":\"twitter\",\"key\":\"\",\"secret\":\"\",\"callback\":\"/auth/twitter/callback\"}}', '', 'object', '', NULL);
INSERT INTO `core_store` VALUES (4, 'plugin_users-permissions_email', '{\"reset_password\":{\"display\":\"Email.template.reset_password\",\"icon\":\"refresh\",\"options\":{\"from\":{\"name\":\"Administration Panel\",\"email\":\"no-reply@strapi.io\"},\"response_email\":\"\",\"object\":\"­Reset password\",\"message\":\"<p>We heard that you lost your password. Sorry about that!</p>\\n\\n<p>But don’t worry! You can use the following link to reset your password:</p>\\n\\n<p><%= URL %>?code=<%= TOKEN %></p>\\n\\n<p>Thanks.</p>\"}}}', '', 'object', '', NULL);
INSERT INTO `core_store` VALUES (5, 'plugin_users-permissions_advanced', '{\"unique_email\":true,\"allow_register\":true,\"default_role\":\"authenticated\"}', '', 'object', '', NULL);

-- ----------------------------
-- Table structure for upload_file
-- ----------------------------
DROP TABLE IF EXISTS `upload_file`;
CREATE TABLE `upload_file`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `hash` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `ext` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `mime` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `size` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `url` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `provider` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for upload_file_morph
-- ----------------------------
DROP TABLE IF EXISTS `upload_file_morph`;
CREATE TABLE `upload_file_morph`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `upload_file_id` int(11) NULL DEFAULT NULL,
  `related_id` int(11) NULL DEFAULT NULL,
  `related_type` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `field` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for users-permissions_permission
-- ----------------------------
DROP TABLE IF EXISTS `users-permissions_permission`;
CREATE TABLE `users-permissions_permission`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role` int(11) NULL DEFAULT NULL,
  `type` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `controller` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `action` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `enabled` tinyint(1) NULL DEFAULT NULL,
  `policy` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 208 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of users-permissions_permission
-- ----------------------------
INSERT INTO `users-permissions_permission` VALUES (1, 1, 'analytics', 'analytics', 'index', 1, '');
INSERT INTO `users-permissions_permission` VALUES (2, 1, 'content-manager', 'contentmanager', 'layout', 1, '');
INSERT INTO `users-permissions_permission` VALUES (3, 1, 'content-manager', 'contentmanager', 'models', 1, '');
INSERT INTO `users-permissions_permission` VALUES (4, 1, 'content-manager', 'contentmanager', 'find', 1, '');
INSERT INTO `users-permissions_permission` VALUES (5, 1, 'content-manager', 'contentmanager', 'count', 1, '');
INSERT INTO `users-permissions_permission` VALUES (6, 1, 'content-manager', 'contentmanager', 'findone', 1, '');
INSERT INTO `users-permissions_permission` VALUES (7, 1, 'content-manager', 'contentmanager', 'create', 1, '');
INSERT INTO `users-permissions_permission` VALUES (8, 1, 'content-manager', 'contentmanager', 'update', 1, '');
INSERT INTO `users-permissions_permission` VALUES (9, 1, 'content-manager', 'contentmanager', 'delete', 1, '');
INSERT INTO `users-permissions_permission` VALUES (10, 1, 'content-type-builder', 'contenttypebuilder', 'getmodels', 1, '');
INSERT INTO `users-permissions_permission` VALUES (11, 1, 'content-type-builder', 'contenttypebuilder', 'getmodel', 1, '');
INSERT INTO `users-permissions_permission` VALUES (12, 1, 'content-type-builder', 'contenttypebuilder', 'getconnections', 1, '');
INSERT INTO `users-permissions_permission` VALUES (13, 1, 'content-type-builder', 'contenttypebuilder', 'createmodel', 1, '');
INSERT INTO `users-permissions_permission` VALUES (14, 1, 'content-type-builder', 'contenttypebuilder', 'updatemodel', 1, '');
INSERT INTO `users-permissions_permission` VALUES (15, 1, 'content-type-builder', 'contenttypebuilder', 'deletemodel', 1, '');
INSERT INTO `users-permissions_permission` VALUES (16, 1, 'content-type-builder', 'contenttypebuilder', 'autoreload', 1, '');
INSERT INTO `users-permissions_permission` VALUES (17, 1, 'content-type-builder', 'contenttypebuilder', 'checktableexists', 1, '');
INSERT INTO `users-permissions_permission` VALUES (18, 1, 'settings-manager', 'settingsmanager', 'menu', 1, '');
INSERT INTO `users-permissions_permission` VALUES (19, 1, 'settings-manager', 'settingsmanager', 'environments', 1, '');
INSERT INTO `users-permissions_permission` VALUES (20, 1, 'settings-manager', 'settingsmanager', 'languages', 1, '');
INSERT INTO `users-permissions_permission` VALUES (21, 1, 'settings-manager', 'settingsmanager', 'databases', 1, '');
INSERT INTO `users-permissions_permission` VALUES (22, 1, 'settings-manager', 'settingsmanager', 'database', 1, '');
INSERT INTO `users-permissions_permission` VALUES (23, 1, 'settings-manager', 'settingsmanager', 'databasemodel', 1, '');
INSERT INTO `users-permissions_permission` VALUES (24, 1, 'settings-manager', 'settingsmanager', 'get', 1, '');
INSERT INTO `users-permissions_permission` VALUES (25, 1, 'settings-manager', 'settingsmanager', 'update', 1, '');
INSERT INTO `users-permissions_permission` VALUES (26, 1, 'settings-manager', 'settingsmanager', 'createlanguage', 1, '');
INSERT INTO `users-permissions_permission` VALUES (27, 1, 'settings-manager', 'settingsmanager', 'deletelanguage', 1, '');
INSERT INTO `users-permissions_permission` VALUES (28, 1, 'settings-manager', 'settingsmanager', 'createdatabase', 1, '');
INSERT INTO `users-permissions_permission` VALUES (29, 1, 'settings-manager', 'settingsmanager', 'updatedatabase', 1, '');
INSERT INTO `users-permissions_permission` VALUES (30, 1, 'settings-manager', 'settingsmanager', 'deletedatabase', 1, '');
INSERT INTO `users-permissions_permission` VALUES (31, 1, 'settings-manager', 'settingsmanager', 'autoreload', 1, '');
INSERT INTO `users-permissions_permission` VALUES (32, 1, 'upload', 'upload', 'upload', 1, '');
INSERT INTO `users-permissions_permission` VALUES (33, 1, 'upload', 'upload', 'getenvironments', 1, '');
INSERT INTO `users-permissions_permission` VALUES (34, 1, 'upload', 'upload', 'getsettings', 1, '');
INSERT INTO `users-permissions_permission` VALUES (35, 1, 'upload', 'upload', 'updatesettings', 1, '');
INSERT INTO `users-permissions_permission` VALUES (36, 1, 'upload', 'upload', 'find', 1, '');
INSERT INTO `users-permissions_permission` VALUES (37, 1, 'upload', 'upload', 'findone', 1, '');
INSERT INTO `users-permissions_permission` VALUES (38, 1, 'upload', 'upload', 'count', 1, '');
INSERT INTO `users-permissions_permission` VALUES (39, 1, 'upload', 'upload', 'destroy', 1, '');
INSERT INTO `users-permissions_permission` VALUES (40, 1, 'upload', 'upload', 'search', 1, '');
INSERT INTO `users-permissions_permission` VALUES (41, 1, 'users-permissions', 'auth', 'callback', 1, '');
INSERT INTO `users-permissions_permission` VALUES (42, 1, 'users-permissions', 'auth', 'changepassword', 1, '');
INSERT INTO `users-permissions_permission` VALUES (43, 1, 'users-permissions', 'auth', 'connect', 1, '');
INSERT INTO `users-permissions_permission` VALUES (44, 1, 'users-permissions', 'auth', 'forgotpassword', 1, '');
INSERT INTO `users-permissions_permission` VALUES (45, 1, 'users-permissions', 'auth', 'register', 1, '');
INSERT INTO `users-permissions_permission` VALUES (46, 1, 'users-permissions', 'user', 'find', 1, '');
INSERT INTO `users-permissions_permission` VALUES (47, 1, 'users-permissions', 'user', 'me', 1, '');
INSERT INTO `users-permissions_permission` VALUES (48, 1, 'users-permissions', 'user', 'findone', 1, '');
INSERT INTO `users-permissions_permission` VALUES (49, 1, 'users-permissions', 'user', 'create', 1, '');
INSERT INTO `users-permissions_permission` VALUES (50, 1, 'users-permissions', 'user', 'update', 1, '');
INSERT INTO `users-permissions_permission` VALUES (51, 1, 'users-permissions', 'user', 'destroy', 1, '');
INSERT INTO `users-permissions_permission` VALUES (52, 1, 'users-permissions', 'userspermissions', 'createrole', 1, '');
INSERT INTO `users-permissions_permission` VALUES (53, 1, 'users-permissions', 'userspermissions', 'deleteprovider', 1, '');
INSERT INTO `users-permissions_permission` VALUES (54, 1, 'users-permissions', 'userspermissions', 'deleterole', 1, '');
INSERT INTO `users-permissions_permission` VALUES (55, 1, 'users-permissions', 'userspermissions', 'getpermissions', 1, '');
INSERT INTO `users-permissions_permission` VALUES (56, 1, 'users-permissions', 'userspermissions', 'getpolicies', 1, '');
INSERT INTO `users-permissions_permission` VALUES (57, 1, 'users-permissions', 'userspermissions', 'getrole', 1, '');
INSERT INTO `users-permissions_permission` VALUES (58, 1, 'users-permissions', 'userspermissions', 'getroles', 1, '');
INSERT INTO `users-permissions_permission` VALUES (59, 1, 'users-permissions', 'userspermissions', 'getroutes', 1, '');
INSERT INTO `users-permissions_permission` VALUES (60, 1, 'users-permissions', 'userspermissions', 'index', 1, '');
INSERT INTO `users-permissions_permission` VALUES (61, 1, 'users-permissions', 'userspermissions', 'init', 1, '');
INSERT INTO `users-permissions_permission` VALUES (62, 1, 'users-permissions', 'userspermissions', 'searchusers', 1, '');
INSERT INTO `users-permissions_permission` VALUES (63, 1, 'users-permissions', 'userspermissions', 'updaterole', 1, '');
INSERT INTO `users-permissions_permission` VALUES (64, 1, 'users-permissions', 'userspermissions', 'getemailtemplate', 1, '');
INSERT INTO `users-permissions_permission` VALUES (65, 1, 'users-permissions', 'userspermissions', 'updateemailtemplate', 1, '');
INSERT INTO `users-permissions_permission` VALUES (66, 1, 'users-permissions', 'userspermissions', 'getadvancedsettings', 1, '');
INSERT INTO `users-permissions_permission` VALUES (67, 1, 'users-permissions', 'userspermissions', 'updateadvancedsettings', 1, '');
INSERT INTO `users-permissions_permission` VALUES (68, 1, 'users-permissions', 'userspermissions', 'getproviders', 1, '');
INSERT INTO `users-permissions_permission` VALUES (69, 1, 'users-permissions', 'userspermissions', 'updateproviders', 1, '');
INSERT INTO `users-permissions_permission` VALUES (70, 2, 'analytics', 'analytics', 'index', 0, '');
INSERT INTO `users-permissions_permission` VALUES (71, 2, 'content-manager', 'contentmanager', 'layout', 0, '');
INSERT INTO `users-permissions_permission` VALUES (72, 2, 'content-manager', 'contentmanager', 'models', 0, '');
INSERT INTO `users-permissions_permission` VALUES (73, 2, 'content-manager', 'contentmanager', 'find', 0, '');
INSERT INTO `users-permissions_permission` VALUES (74, 2, 'content-manager', 'contentmanager', 'count', 0, '');
INSERT INTO `users-permissions_permission` VALUES (75, 2, 'content-manager', 'contentmanager', 'findone', 0, '');
INSERT INTO `users-permissions_permission` VALUES (76, 2, 'content-manager', 'contentmanager', 'create', 0, '');
INSERT INTO `users-permissions_permission` VALUES (77, 2, 'content-manager', 'contentmanager', 'update', 0, '');
INSERT INTO `users-permissions_permission` VALUES (78, 2, 'content-manager', 'contentmanager', 'delete', 0, '');
INSERT INTO `users-permissions_permission` VALUES (79, 2, 'content-type-builder', 'contenttypebuilder', 'getmodels', 0, '');
INSERT INTO `users-permissions_permission` VALUES (80, 2, 'content-type-builder', 'contenttypebuilder', 'getmodel', 0, '');
INSERT INTO `users-permissions_permission` VALUES (81, 2, 'content-type-builder', 'contenttypebuilder', 'getconnections', 0, '');
INSERT INTO `users-permissions_permission` VALUES (82, 2, 'content-type-builder', 'contenttypebuilder', 'createmodel', 0, '');
INSERT INTO `users-permissions_permission` VALUES (83, 2, 'content-type-builder', 'contenttypebuilder', 'updatemodel', 0, '');
INSERT INTO `users-permissions_permission` VALUES (84, 2, 'content-type-builder', 'contenttypebuilder', 'deletemodel', 0, '');
INSERT INTO `users-permissions_permission` VALUES (85, 2, 'content-type-builder', 'contenttypebuilder', 'autoreload', 1, '');
INSERT INTO `users-permissions_permission` VALUES (86, 2, 'content-type-builder', 'contenttypebuilder', 'checktableexists', 0, '');
INSERT INTO `users-permissions_permission` VALUES (87, 2, 'settings-manager', 'settingsmanager', 'menu', 0, '');
INSERT INTO `users-permissions_permission` VALUES (88, 2, 'settings-manager', 'settingsmanager', 'environments', 0, '');
INSERT INTO `users-permissions_permission` VALUES (89, 2, 'settings-manager', 'settingsmanager', 'languages', 0, '');
INSERT INTO `users-permissions_permission` VALUES (90, 2, 'settings-manager', 'settingsmanager', 'databases', 0, '');
INSERT INTO `users-permissions_permission` VALUES (91, 2, 'settings-manager', 'settingsmanager', 'database', 0, '');
INSERT INTO `users-permissions_permission` VALUES (92, 2, 'settings-manager', 'settingsmanager', 'databasemodel', 0, '');
INSERT INTO `users-permissions_permission` VALUES (93, 2, 'settings-manager', 'settingsmanager', 'get', 0, '');
INSERT INTO `users-permissions_permission` VALUES (94, 2, 'settings-manager', 'settingsmanager', 'update', 0, '');
INSERT INTO `users-permissions_permission` VALUES (95, 2, 'settings-manager', 'settingsmanager', 'createlanguage', 0, '');
INSERT INTO `users-permissions_permission` VALUES (96, 2, 'settings-manager', 'settingsmanager', 'deletelanguage', 0, '');
INSERT INTO `users-permissions_permission` VALUES (97, 2, 'settings-manager', 'settingsmanager', 'createdatabase', 0, '');
INSERT INTO `users-permissions_permission` VALUES (98, 2, 'settings-manager', 'settingsmanager', 'updatedatabase', 0, '');
INSERT INTO `users-permissions_permission` VALUES (99, 2, 'settings-manager', 'settingsmanager', 'deletedatabase', 0, '');
INSERT INTO `users-permissions_permission` VALUES (100, 2, 'settings-manager', 'settingsmanager', 'autoreload', 1, '');
INSERT INTO `users-permissions_permission` VALUES (101, 2, 'upload', 'upload', 'upload', 0, '');
INSERT INTO `users-permissions_permission` VALUES (102, 2, 'upload', 'upload', 'getenvironments', 0, '');
INSERT INTO `users-permissions_permission` VALUES (103, 2, 'upload', 'upload', 'getsettings', 0, '');
INSERT INTO `users-permissions_permission` VALUES (104, 2, 'upload', 'upload', 'updatesettings', 0, '');
INSERT INTO `users-permissions_permission` VALUES (105, 2, 'upload', 'upload', 'find', 0, '');
INSERT INTO `users-permissions_permission` VALUES (106, 2, 'upload', 'upload', 'findone', 0, '');
INSERT INTO `users-permissions_permission` VALUES (107, 2, 'upload', 'upload', 'count', 0, '');
INSERT INTO `users-permissions_permission` VALUES (108, 2, 'upload', 'upload', 'destroy', 0, '');
INSERT INTO `users-permissions_permission` VALUES (109, 2, 'upload', 'upload', 'search', 0, '');
INSERT INTO `users-permissions_permission` VALUES (110, 2, 'users-permissions', 'auth', 'callback', 0, '');
INSERT INTO `users-permissions_permission` VALUES (111, 2, 'users-permissions', 'auth', 'changepassword', 0, '');
INSERT INTO `users-permissions_permission` VALUES (112, 2, 'users-permissions', 'auth', 'connect', 1, '');
INSERT INTO `users-permissions_permission` VALUES (113, 2, 'users-permissions', 'auth', 'forgotpassword', 0, '');
INSERT INTO `users-permissions_permission` VALUES (114, 2, 'users-permissions', 'auth', 'register', 0, '');
INSERT INTO `users-permissions_permission` VALUES (115, 2, 'users-permissions', 'user', 'find', 0, '');
INSERT INTO `users-permissions_permission` VALUES (116, 2, 'users-permissions', 'user', 'me', 1, '');
INSERT INTO `users-permissions_permission` VALUES (117, 2, 'users-permissions', 'user', 'findone', 0, '');
INSERT INTO `users-permissions_permission` VALUES (118, 2, 'users-permissions', 'user', 'create', 0, '');
INSERT INTO `users-permissions_permission` VALUES (119, 2, 'users-permissions', 'user', 'update', 0, '');
INSERT INTO `users-permissions_permission` VALUES (120, 2, 'users-permissions', 'user', 'destroy', 0, '');
INSERT INTO `users-permissions_permission` VALUES (121, 2, 'users-permissions', 'userspermissions', 'createrole', 0, '');
INSERT INTO `users-permissions_permission` VALUES (122, 2, 'users-permissions', 'userspermissions', 'deleteprovider', 0, '');
INSERT INTO `users-permissions_permission` VALUES (123, 2, 'users-permissions', 'userspermissions', 'deleterole', 0, '');
INSERT INTO `users-permissions_permission` VALUES (124, 2, 'users-permissions', 'userspermissions', 'getpermissions', 0, '');
INSERT INTO `users-permissions_permission` VALUES (125, 2, 'users-permissions', 'userspermissions', 'getpolicies', 0, '');
INSERT INTO `users-permissions_permission` VALUES (126, 2, 'users-permissions', 'userspermissions', 'getrole', 0, '');
INSERT INTO `users-permissions_permission` VALUES (127, 2, 'users-permissions', 'userspermissions', 'getroles', 0, '');
INSERT INTO `users-permissions_permission` VALUES (128, 2, 'users-permissions', 'userspermissions', 'getroutes', 0, '');
INSERT INTO `users-permissions_permission` VALUES (129, 2, 'users-permissions', 'userspermissions', 'index', 0, '');
INSERT INTO `users-permissions_permission` VALUES (130, 2, 'users-permissions', 'userspermissions', 'init', 1, '');
INSERT INTO `users-permissions_permission` VALUES (131, 2, 'users-permissions', 'userspermissions', 'searchusers', 0, '');
INSERT INTO `users-permissions_permission` VALUES (132, 2, 'users-permissions', 'userspermissions', 'updaterole', 0, '');
INSERT INTO `users-permissions_permission` VALUES (133, 2, 'users-permissions', 'userspermissions', 'getemailtemplate', 0, '');
INSERT INTO `users-permissions_permission` VALUES (134, 2, 'users-permissions', 'userspermissions', 'updateemailtemplate', 0, '');
INSERT INTO `users-permissions_permission` VALUES (135, 2, 'users-permissions', 'userspermissions', 'getadvancedsettings', 0, '');
INSERT INTO `users-permissions_permission` VALUES (136, 2, 'users-permissions', 'userspermissions', 'updateadvancedsettings', 0, '');
INSERT INTO `users-permissions_permission` VALUES (137, 2, 'users-permissions', 'userspermissions', 'getproviders', 0, '');
INSERT INTO `users-permissions_permission` VALUES (138, 2, 'users-permissions', 'userspermissions', 'updateproviders', 0, '');
INSERT INTO `users-permissions_permission` VALUES (139, 3, 'analytics', 'analytics', 'index', 0, '');
INSERT INTO `users-permissions_permission` VALUES (140, 3, 'content-manager', 'contentmanager', 'layout', 0, '');
INSERT INTO `users-permissions_permission` VALUES (141, 3, 'content-manager', 'contentmanager', 'models', 0, '');
INSERT INTO `users-permissions_permission` VALUES (142, 3, 'content-manager', 'contentmanager', 'find', 0, '');
INSERT INTO `users-permissions_permission` VALUES (143, 3, 'content-manager', 'contentmanager', 'count', 0, '');
INSERT INTO `users-permissions_permission` VALUES (144, 3, 'content-manager', 'contentmanager', 'findone', 0, '');
INSERT INTO `users-permissions_permission` VALUES (145, 3, 'content-manager', 'contentmanager', 'create', 0, '');
INSERT INTO `users-permissions_permission` VALUES (146, 3, 'content-manager', 'contentmanager', 'update', 0, '');
INSERT INTO `users-permissions_permission` VALUES (147, 3, 'content-manager', 'contentmanager', 'delete', 0, '');
INSERT INTO `users-permissions_permission` VALUES (148, 3, 'content-type-builder', 'contenttypebuilder', 'getmodels', 0, '');
INSERT INTO `users-permissions_permission` VALUES (149, 3, 'content-type-builder', 'contenttypebuilder', 'getmodel', 0, '');
INSERT INTO `users-permissions_permission` VALUES (150, 3, 'content-type-builder', 'contenttypebuilder', 'getconnections', 0, '');
INSERT INTO `users-permissions_permission` VALUES (151, 3, 'content-type-builder', 'contenttypebuilder', 'createmodel', 0, '');
INSERT INTO `users-permissions_permission` VALUES (152, 3, 'content-type-builder', 'contenttypebuilder', 'updatemodel', 0, '');
INSERT INTO `users-permissions_permission` VALUES (153, 3, 'content-type-builder', 'contenttypebuilder', 'deletemodel', 0, '');
INSERT INTO `users-permissions_permission` VALUES (154, 3, 'content-type-builder', 'contenttypebuilder', 'autoreload', 1, '');
INSERT INTO `users-permissions_permission` VALUES (155, 3, 'content-type-builder', 'contenttypebuilder', 'checktableexists', 0, '');
INSERT INTO `users-permissions_permission` VALUES (156, 3, 'settings-manager', 'settingsmanager', 'menu', 0, '');
INSERT INTO `users-permissions_permission` VALUES (157, 3, 'settings-manager', 'settingsmanager', 'environments', 0, '');
INSERT INTO `users-permissions_permission` VALUES (158, 3, 'settings-manager', 'settingsmanager', 'languages', 0, '');
INSERT INTO `users-permissions_permission` VALUES (159, 3, 'settings-manager', 'settingsmanager', 'databases', 0, '');
INSERT INTO `users-permissions_permission` VALUES (160, 3, 'settings-manager', 'settingsmanager', 'database', 0, '');
INSERT INTO `users-permissions_permission` VALUES (161, 3, 'settings-manager', 'settingsmanager', 'databasemodel', 0, '');
INSERT INTO `users-permissions_permission` VALUES (162, 3, 'settings-manager', 'settingsmanager', 'get', 0, '');
INSERT INTO `users-permissions_permission` VALUES (163, 3, 'settings-manager', 'settingsmanager', 'update', 0, '');
INSERT INTO `users-permissions_permission` VALUES (164, 3, 'settings-manager', 'settingsmanager', 'createlanguage', 0, '');
INSERT INTO `users-permissions_permission` VALUES (165, 3, 'settings-manager', 'settingsmanager', 'deletelanguage', 0, '');
INSERT INTO `users-permissions_permission` VALUES (166, 3, 'settings-manager', 'settingsmanager', 'createdatabase', 0, '');
INSERT INTO `users-permissions_permission` VALUES (167, 3, 'settings-manager', 'settingsmanager', 'updatedatabase', 0, '');
INSERT INTO `users-permissions_permission` VALUES (168, 3, 'settings-manager', 'settingsmanager', 'deletedatabase', 0, '');
INSERT INTO `users-permissions_permission` VALUES (169, 3, 'settings-manager', 'settingsmanager', 'autoreload', 1, '');
INSERT INTO `users-permissions_permission` VALUES (170, 3, 'upload', 'upload', 'upload', 0, '');
INSERT INTO `users-permissions_permission` VALUES (171, 3, 'upload', 'upload', 'getenvironments', 0, '');
INSERT INTO `users-permissions_permission` VALUES (172, 3, 'upload', 'upload', 'getsettings', 0, '');
INSERT INTO `users-permissions_permission` VALUES (173, 3, 'upload', 'upload', 'updatesettings', 0, '');
INSERT INTO `users-permissions_permission` VALUES (174, 3, 'upload', 'upload', 'find', 0, '');
INSERT INTO `users-permissions_permission` VALUES (175, 3, 'upload', 'upload', 'findone', 0, '');
INSERT INTO `users-permissions_permission` VALUES (176, 3, 'upload', 'upload', 'count', 0, '');
INSERT INTO `users-permissions_permission` VALUES (177, 3, 'upload', 'upload', 'destroy', 0, '');
INSERT INTO `users-permissions_permission` VALUES (178, 3, 'upload', 'upload', 'search', 0, '');
INSERT INTO `users-permissions_permission` VALUES (179, 3, 'users-permissions', 'auth', 'callback', 1, '');
INSERT INTO `users-permissions_permission` VALUES (180, 3, 'users-permissions', 'auth', 'changepassword', 1, '');
INSERT INTO `users-permissions_permission` VALUES (181, 3, 'users-permissions', 'auth', 'connect', 1, '');
INSERT INTO `users-permissions_permission` VALUES (182, 3, 'users-permissions', 'auth', 'forgotpassword', 1, '');
INSERT INTO `users-permissions_permission` VALUES (183, 3, 'users-permissions', 'auth', 'register', 1, '');
INSERT INTO `users-permissions_permission` VALUES (184, 3, 'users-permissions', 'user', 'find', 0, '');
INSERT INTO `users-permissions_permission` VALUES (185, 3, 'users-permissions', 'user', 'me', 1, '');
INSERT INTO `users-permissions_permission` VALUES (186, 3, 'users-permissions', 'user', 'findone', 0, '');
INSERT INTO `users-permissions_permission` VALUES (187, 3, 'users-permissions', 'user', 'create', 0, '');
INSERT INTO `users-permissions_permission` VALUES (188, 3, 'users-permissions', 'user', 'update', 0, '');
INSERT INTO `users-permissions_permission` VALUES (189, 3, 'users-permissions', 'user', 'destroy', 0, '');
INSERT INTO `users-permissions_permission` VALUES (190, 3, 'users-permissions', 'userspermissions', 'createrole', 0, '');
INSERT INTO `users-permissions_permission` VALUES (191, 3, 'users-permissions', 'userspermissions', 'deleteprovider', 0, '');
INSERT INTO `users-permissions_permission` VALUES (192, 3, 'users-permissions', 'userspermissions', 'deleterole', 0, '');
INSERT INTO `users-permissions_permission` VALUES (193, 3, 'users-permissions', 'userspermissions', 'getpermissions', 0, '');
INSERT INTO `users-permissions_permission` VALUES (194, 3, 'users-permissions', 'userspermissions', 'getpolicies', 0, '');
INSERT INTO `users-permissions_permission` VALUES (195, 3, 'users-permissions', 'userspermissions', 'getrole', 0, '');
INSERT INTO `users-permissions_permission` VALUES (196, 3, 'users-permissions', 'userspermissions', 'getroles', 0, '');
INSERT INTO `users-permissions_permission` VALUES (197, 3, 'users-permissions', 'userspermissions', 'getroutes', 0, '');
INSERT INTO `users-permissions_permission` VALUES (198, 3, 'users-permissions', 'userspermissions', 'index', 0, '');
INSERT INTO `users-permissions_permission` VALUES (199, 3, 'users-permissions', 'userspermissions', 'init', 1, '');
INSERT INTO `users-permissions_permission` VALUES (200, 3, 'users-permissions', 'userspermissions', 'searchusers', 0, '');
INSERT INTO `users-permissions_permission` VALUES (201, 3, 'users-permissions', 'userspermissions', 'updaterole', 0, '');
INSERT INTO `users-permissions_permission` VALUES (202, 3, 'users-permissions', 'userspermissions', 'getemailtemplate', 0, '');
INSERT INTO `users-permissions_permission` VALUES (203, 3, 'users-permissions', 'userspermissions', 'updateemailtemplate', 0, '');
INSERT INTO `users-permissions_permission` VALUES (204, 3, 'users-permissions', 'userspermissions', 'getadvancedsettings', 0, '');
INSERT INTO `users-permissions_permission` VALUES (205, 3, 'users-permissions', 'userspermissions', 'updateadvancedsettings', 0, '');
INSERT INTO `users-permissions_permission` VALUES (206, 3, 'users-permissions', 'userspermissions', 'getproviders', 0, '');
INSERT INTO `users-permissions_permission` VALUES (207, 3, 'users-permissions', 'userspermissions', 'updateproviders', 0, '');

-- ----------------------------
-- Table structure for users-permissions_role
-- ----------------------------
DROP TABLE IF EXISTS `users-permissions_role`;
CREATE TABLE `users-permissions_role`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `description` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `type` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of users-permissions_role
-- ----------------------------
INSERT INTO `users-permissions_role` VALUES (1, 'Administrator', 'These users have all access in the project.', 'root');
INSERT INTO `users-permissions_role` VALUES (2, 'Authenticated', 'Default role given to authenticated user.', 'authenticated');
INSERT INTO `users-permissions_role` VALUES (3, 'Public', 'Default role given to unauthenticated user.', 'public');

-- ----------------------------
-- Table structure for users-permissions_user
-- ----------------------------
DROP TABLE IF EXISTS `users-permissions_user`;
CREATE TABLE `users-permissions_user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `email` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `provider` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `role` int(11) NULL DEFAULT NULL,
  `resetPasswordToken` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `password` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of users-permissions_user
-- ----------------------------
INSERT INTO `users-permissions_user` VALUES (1, 'admin', 'admin@gmail.com', 'local', 1, NULL, '$2a$10$VGoigyzEgShp/i.jm4MhFO5PDUu.7ucZ7ctb6HRevvXzeiY7IP00q', '2018-05-03 11:55:07', '0000-00-00 00:00:00');

SET FOREIGN_KEY_CHECKS = 1;
